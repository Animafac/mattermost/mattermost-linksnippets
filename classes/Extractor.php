<?php

namespace MattermostLinksnippets;

use Fusonic\OpenGraph\Consumer;
use Symfony\Component\DomCrawler\Crawler;

class Extractor
{

    private $consumer;
    private $client;

    public function __construct()
    {
        $this->consumer = new Consumer();
        $this->client = new \GuzzleHttp\Client();
    }

    public function getMarkdown($url)
    {
        $res = $this->client->get($url);
        $html = (string) $res->getBody();
        $ogp = $this->consumer->loadHtml($html);
        $crawler = new Crawler($html);
        $node = $crawler->filter('title');
        if (isset($ogp->title)) {
            $title = $ogp->title;
        } elseif (count($node) > 0) {
            $title = $node->text();
        }
        if (isset($ogp->url)) {
            $url = $ogp->url;
        }
        $node = $crawler->filter('meta[name=description]');
        if (isset($ogp->description)) {
            $description = $ogp->description;
        } elseif (count($node) > 0) {
            $description = $node->attr('content');
        }
        if (isset($ogp->images) && !empty($ogp->images)) {
            $image = $ogp->images[0]->url;
        }
        if (isset($title)) {
            $markdown = '**['.$title.']('.$url.')**';
            if (isset($description)) {
                $markdown .= PHP_EOL.$description;
            }
            if (isset($image)) {
                $markdown .= PHP_EOL.PHP_EOL.'Image ⟶ '.$image;
            }
            return $markdown;
        }
    }
}
