<?php
namespace MattermostLinksnippets;

require_once __DIR__.'/vendor/autoload.php';

use Slim\App;
use MattermostLinksnippets\Controller\HookController;

$controller = new HookController();

$app = new App;
$app->post('/', array($controller, 'main'));
$app->run();
