<?php

namespace MattermostLinksnippets\Test;

use MattermostLinksnippets\Extractor;

class ExtractorTest extends \PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        $this->extractor = new Extractor();
    }

    /**
     * @dataProvider urlProvider
     */
    public function testGetMarkdown($url, $markdown)
    {
        $this->assertEquals($markdown, $this->extractor->getMarkdown($url));
    }

    public function urlProvider()
    {
        return array(
            array(
                'https://www.youtube.com/watch?v=M7IpKCZ47pU',
                "**[It's Not Me, It's You - Hearts Under Fire](https://www.youtube.com/watch?v=M7IpKCZ47pU)**".PHP_EOL.
                "First ever Hearts Under Fire music video by James Watt Website: http://www.heartsunderfire.co.uk/".
                " Facebook: http://www.facebook.com/heartsunderfire/ Twitter:...".PHP_EOL.PHP_EOL.
                'Image ⟶ https://i.ytimg.com/vi/M7IpKCZ47pU/maxresdefault.jpg'
            ),
            array(
                'https://vimeo.com/24195442',
                "**[Carving the Mountains](https://vimeo.com/24195442)**".PHP_EOL.
                "Una tarde patinando en la sierra de Madrid A spring afternoon in the Madrid Mountains.".
                '  Con la chicas de Longboard Girls Crew '.
                'http://www.facebook.com/longboardgc  Música: Pascal…'.PHP_EOL.PHP_EOL.
                'Image ⟶ https://i.vimeocdn.com/video/158281254_1280x720.jpg'
            ),
            array(
                'https://www.rudloff.pro/',
                "**[Pierre Rudloff](https://www.rudloff.pro/)**".PHP_EOL.
                "Développeur + associatif + formateur".PHP_EOL.PHP_EOL.
                'Image ⟶ https://www.rudloff.pro/images/logo_rudloff_social.jpg'
            ),
            array(
                'https://www.animafac.net/actualites/comment-demander-cesure-mon-etablissement/',
                "**[Votre université met-elle en place la césure ? - Animafac]".
                "(https://www.animafac.net/actualites/comment-demander-cesure-mon-etablissement/)**".PHP_EOL.
                'Comment faire une année de césure ? '.
                'Retrouvez ici un kit avec toutes les infos nécessaires.'.PHP_EOL.PHP_EOL.
                'Image ⟶ https://www.animafac.net/media/tableau-cesure-e1467817024671.png'
            ),
            array(
                'https://tools.animafac.net/',
                "**[Animatools](https://tools.animafac.net/)**"
            ),
            array(
                'http://emoji.netlib.re/',
                "**[EmojiBoard](http://emoji.netlib.re/)**
Simple emoji search list"
            ),
            array(
                'https://www.rudloff.pro/images/logo_rudloff_big.png',
                null
            ),
            array(
                'http://www.nextinpact.com/news/100719-windows-10-pourquoi-cnil-met-en-demeure-microsoft.htm',
                '**[Windows 10 : pourquoi la CNIL met en demeure '.
                'Microsoft](http://www.nextinpact.com/news/100719-windows-10-pourquoi-'.
                'cnil-met-en-demeure-microsoft.htm)**'.PHP_EOL.
                'De nombreux manquements à la loi Informatique et Libertés de 1978. '.
                'Voilà le reproche qu’a adressé hier en fin de journée la CNIL à Microsoft, '.
                'et son système d’exploitation Windows 10. L’éditeur a trois mois pour corriger le tir, '.
                'avant une possible sanction.'.PHP_EOL.PHP_EOL.
                'Image ⟶ http://cdn2.nextinpact.com/images/bd/wide-linked-media/8069.jpg'
            ),
            array(
                'https://giphy.com/gifs/mrw-account-panties-inyqrgp9o3NUA',
                '**[Fun GIF - Find & Share on GIPHY](https://media.giphy.com/media/inyqrgp9o3NUA/giphy.gif)**'.PHP_EOL.
                'Discover & Share this Fun GIF with everyone you know. '.
                'GIPHY is how you search, share, discover, and create GIFs.'.PHP_EOL.PHP_EOL.
                'Image ⟶ https://media.giphy.com/media/inyqrgp9o3NUA/giphy.gif'
            )
        );
    }

    /**
     * @dataProvider ErrorUrlProvider
     * @expectedException Exception
     */
    public function testGetMarkdownError($url)
    {
        $this->extractor->getMarkdown($url);
    }

    public function errorUrlProvider()
    {
        return array(
            array(
                'foo'
            ),
            array(
                'http://foo.bar'
            )
        );
    }
}
