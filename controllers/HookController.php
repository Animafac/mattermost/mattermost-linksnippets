<?php

namespace MattermostLinksnippets\Controller;

use Slim\Http\Request;
use Slim\Http\Response;
use MattermostLinksnippets\Extractor;

class HookController
{

    public function main(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        try {
            $extractor = new Extractor();
            $markdown = $extractor->getMarkdown($data['text']);
            $return = array('text'=>$markdown, 'icon_url'=>$request->getUri()->getBaseUrl().'/img/icon.png');
            if (isset($data['user_name'])) {
                $return['username'] = $data['user_name'];
            }
            if (isset($markdown)) {
                return $response->withJson($return);
            }
        } catch (\Exception $e) {
            if (isset($data['debug'])) {
                return $response->withJson(array('error'=>$e->getMessage()));
            } else {
                return;
            }
        }
    }
}
