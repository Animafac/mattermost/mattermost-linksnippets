# mattermost-linksnippets

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/) This functionnality is now available directly in Mattermost 4.4.

Mattermost outgoing webhook that adds link snippets using OpenGraph metadata

## Usage

Add an outgoing webhook with the following settings:

* Trigger word: `http`
* URL: [https://example.com/mattermost-linksnippets/](https://example.com/mattermost-linksnippets/)
* Tigger when: First word starts with a trigger word
* Content type: `application/x-www-form-urlencode`

## Setup

```bash
composer install
```
